package com.almacen.ingresopersona.repository;

import org.springframework.data.repository.CrudRepository;

import com.almacen.ingresopersona.entity.Persona;

public interface PersonaRepository extends CrudRepository<Persona, Integer>{

}
