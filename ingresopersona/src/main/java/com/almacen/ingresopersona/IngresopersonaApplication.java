package com.almacen.ingresopersona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngresopersonaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IngresopersonaApplication.class, args);
	}
}
