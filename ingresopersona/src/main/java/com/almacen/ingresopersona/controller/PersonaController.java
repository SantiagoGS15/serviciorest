package com.almacen.ingresopersona.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.almacen.ingresopersona.entity.Persona;
import com.almacen.ingresopersona.repository.PersonaRepository;

@Controller
public class PersonaController {

	@Autowired
	PersonaRepository personaRepository;

	@PostMapping("/crearPersona")
	public String crearPersona(Model model, Persona persona) {
		personaRepository.save(persona);
		model.addAttribute("persona", new Persona());
		model.addAttribute("personas", personaRepository.findAll());
		return "index";

	}

	@GetMapping
	public String index(Model model, Persona persona) {
		model.addAttribute("persona", new Persona());
		model.addAttribute("personas", personaRepository.findAll());
		return "index";
	}
}
